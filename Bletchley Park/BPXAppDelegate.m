//
//  BPXAppDelegate.m
//  Bletchley Park
//
//  Created by Richard George on 21/12/2011.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "BPXAppDelegate.h"


#import "BPXMainMenuViewController.h"

@implementation BPXAppDelegate

@synthesize window = _window;
@synthesize tabBarController = _tabBarController;


#pragma mark - convenience methods
/*
- (NSArray *)initialiseTabViewControllers
{
    //Places
    //People
    //Timeline?
    //Visiting - directions, opening times
    //Enigma (Machines?)
     // Keys, machines via people, places only? Search page?
    
    UIViewController *firstControllerInTab=nil;
    UINavigationController *navController=nil;
    NSMutableArray *controllers=nil;
    controllers = [[[NSMutableArray alloc] init] autorelease];
    //Pop "first page" controllers of each tab into array here:
    
//    firstControllerInTab = [[[BPXFirstViewController alloc] initWithNibName:@"BPXFirstViewController" bundle:nil] autorelease];
//    navController =  [[[UINavigationController alloc] initWithRootViewController:firstControllerInTab] autorelease];
//    //Templated app sets title, image within controller init by default
//     // Navcontroller seems to take title / tabBarItem from contained controller.
////    navController.title = @"F1rst";
////    navController.tabBarItem.image = [UIImage imageNamed:@""];
//    [controllers addObject:navController];
//    
//    
//    firstControllerInTab = [[[BPXSecondViewController alloc] initWithNibName:@"BPXSecondViewController" bundle:nil] autorelease];
//    navController =  [[[UINavigationController alloc] initWithRootViewController:firstControllerInTab] autorelease];
//    [controllers addObject:navController];
//    

    return [NSArray arrayWithArray:controllers]; //immutable!
}
 */

#pragma mark - cleanup

- (void)dealloc
{
    [_window release];
    [_tabBarController release];
    [super dealloc];
}

#pragma mark - lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.

//    self.tabBarController = [[[UITabBarController alloc] init] autorelease];
//    self.tabBarController.viewControllers = [self initialiseTabViewControllers];
    
    self.window.rootViewController = [[[BPXMainMenuViewController alloc] initWithNibName:@"BPXMainMenuViewController" bundle:nil] autorelease];
    [self.window makeKeyAndVisible];
    
#ifdef UseSM3MapView
        [SM3DARMapView class];
#endif
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
}
*/

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed
{
}
*/

@end
