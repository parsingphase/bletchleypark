//
//  BPXLocationPoint.h
//  Bletchley Park
//
//  Created by Richard George on 03/01/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface BPXLocationPoint : NSObject <MKAnnotation>
{
    NSString *name_;
    NSNumber *latitude_;
    NSNumber *longitude_;
}

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSNumber *latitude;
@property (nonatomic, retain) NSNumber *longitude;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

@end
