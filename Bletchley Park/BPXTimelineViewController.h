//
//  BPXTimelineViewController.h
//  Bletchley Park
//
//  Created by Richard George on 07/01/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BPXTimelineViewController : UITableViewController
{
    NSArray *years_;
    NSDictionary *eventsByYear_;
}

@property (nonatomic, retain) NSArray *years;
@property (nonatomic, retain) NSDictionary *eventsByYear;

@end
