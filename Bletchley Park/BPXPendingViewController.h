//
//  BPXPendingViewController.h
//  Bletchley Park
//
//  Created by Richard George on 30/12/2011.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BPXPendingViewController : UIViewController
@property (retain, nonatomic) IBOutlet UILabel *infoLabel;

@end
