//
//  BPXNavigationController.m
//  Bletchley Park
//
//  Created by Richard George on 30/12/2011.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "BPXNavigationController.h"

@implementation BPXNavigationController


- (id)initWithRootViewController:(UIViewController *)rootViewController
{
    id returnable = [super initWithRootViewController:rootViewController];
    self.navigationBar.tintColor = [UIColor colorWithRed:0.7 green:0.6 blue:0.4 alpha:1.0];
    //not a bad guess!
    
    rootViewController.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc]
                                                            initWithImage:[UIImage imageNamed:@"mini-white-38-house"] style:UIBarButtonItemStyleBordered
                                                            target:self action:@selector(doneTapped:)] autorelease];
    
    return returnable;
}

- (void)doneTapped:(id)sender 
{
    PSLogDebug(@"BPXNavigationController> doneTapped; parent controller: '%@'", self.parentViewController);
    
    //Could either be in a tab controller or solo:
    if(self.parentViewController) {
        [self.parentViewController dismissModalViewControllerAnimated:YES];
    } else {
        [self dismissModalViewControllerAnimated:YES];
    }
}

@end
