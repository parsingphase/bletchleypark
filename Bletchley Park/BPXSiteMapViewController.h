//
//  BPXSiteMapViewController.h
//  Bletchley Park
//
//  Created by Richard George on 30/12/2011.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Mapkit/MapKit.h>
#import "CHCSV.h"
#import "BPXMapView.h"
#import "BPXLocationPoint.h"


@interface BPXSiteMapViewController : UIViewController <CHCSVParserDelegate,UIPickerViewDelegate, UIPickerViewDataSource,MKMapViewDelegate, CLLocationManagerDelegate> {
    NSString *pointsFileName_;
    NSArray *points_;
    NSMutableArray *loadingPoints_; 
    CLLocationManager *locationManager_;
    CLLocation *location_;
    CLHeading *heading_;
    BPXLocationPoint *destinationPoint_;

#ifdef UseSM3MapView
    BOOL inVrMode_;
#endif
}
//@property (retain, nonatomic) IBOutlet UILabel *locationTitleLabel;
@property (retain, nonatomic) IBOutlet UIButton *locationSelectButton;
- (IBAction)locationSelectButtonPressed:(id)sender;
@property (retain, nonatomic) IBOutlet BPXMapView *mapView;
@property (retain, nonatomic) IBOutlet UIPickerView *locationSelector;
@property (retain, nonatomic) IBOutlet UIImageView *directionImageView;

@property (nonatomic, retain) NSString *pointsfileName;
@property (nonatomic, retain) NSArray *points;
@property (nonatomic, retain) NSMutableArray *loadingPoints;
@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, retain) CLLocation *location;
@property (nonatomic, retain) CLHeading *heading;
@property (nonatomic, retain) BPXLocationPoint *destinationPoint;
#ifdef UseSM3MapView
@property (nonatomic, assign) BOOL inVrMode;
#endif

//- (IBAction)tappedViewToggleButton:(id)sender;
- (void) enableLocationServices;
- (void) disableLocationServices;
- (void) deviceDidMoveOrTurn;
- (void) scrollMapToIncludePoint: (BPXLocationPoint *)point;
- (void) scrollMapToDefaultPositionAnimated:(BOOL) animated;

@end
