//
//  BPXTextAreaViewController.h
//  Bletchley Park
//
//  Created by Richard George on 31/12/2011.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BPXTextAreaViewController : UIViewController {
    NSString *displayText_;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil textFile:(NSString *)path;

@property (retain, nonatomic) IBOutlet UITextView *textView;
@property (retain, nonatomic) NSString *displayText;

@end
