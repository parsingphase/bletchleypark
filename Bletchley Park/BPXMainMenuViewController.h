//
//  BPXMainMenuViewController.h
//  Bletchley Park
//
//  Created by Richard George on 30/12/2011.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BPXMainMenuViewController : UIViewController
@property (retain, nonatomic) IBOutlet UIButton *aboutButton;
@property (retain, nonatomic) IBOutlet UIButton *aroundParkButton;
@property (retain, nonatomic) IBOutlet UIButton *visitButton;
@property (retain, nonatomic) IBOutlet UIButton *eventsButton;
@property (retain, nonatomic) IBOutlet UIButton *supportButton;
@property (retain, nonatomic) IBOutlet UILabel *buildLabel;

- (IBAction)aboutButtonPressed:(id)sender;
- (IBAction)aroundButtonPressed:(id)sender;
- (IBAction)visitButtonPressed:(id)sender;
- (IBAction)eventsButtonPressed:(id)sender;
- (IBAction)supportButtonPressed:(id)sender;
- (IBAction)infoButtonPressed:(id)sender;


@end
