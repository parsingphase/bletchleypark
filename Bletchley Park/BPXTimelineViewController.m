//
//  BPXTimelineViewController.m
//  Bletchley Park
//
//  Created by Richard George on 07/01/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BPXTimelineViewController.h"
#import "CHCSV.h"

@implementation BPXTimelineViewController
@synthesize years = years_;
@synthesize eventsByYear = eventsByYear_;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.years = [NSArray arrayWithObjects:
                  @"1883",
                  @"1914",@"1926",@"1927",@"1931",@"1932",@"1936",@"1937",@"1938",
                  @"1939",@"1940",@"1941",@"1942",@"1943",@"1944",@"1945",    
                  @"1974",@"1987",@"1992",@"1993",@"2006",@"2008",
                  nil];
    
    NSMutableDictionary *loadingDictionary = [NSMutableDictionary dictionaryWithCapacity:[self.years count]];
    
    NSString *filePath;
    NSError *error;
    NSArray *yearEvents;
    for (NSString *year in self.years) {
        filePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"events%@",year] ofType:@"csv"];
        
        if(filePath) {
            PSLogDebug(@"Load events for %@", year);
            yearEvents = [NSArray arrayWithContentsOfCSVFile:filePath encoding:NSUTF8StringEncoding error:&error];
            if(yearEvents) {
                PSLogDebug(@"Found %d events for %@", [yearEvents count],year);
                if([yearEvents count]) {
                    [loadingDictionary setObject:yearEvents forKey:year];
                } 
            } else {
                PSLog(@"Got loading error: %@", error);
            }
        }
    }
    
    self.eventsByYear = [NSDictionary dictionaryWithDictionary:loadingDictionary];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [self.years count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [self.years objectAtIndex:section];
}

/* */
// http://www.iphonedevsdk.com/forum/iphone-sdk-development/5172-font-size-color-tableview-header.html#post75912
- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 22.0;
}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    CGRect frame = CGRectMake(0.0, 0.0, tableView.bounds.size.width, [self tableView:tableView heightForHeaderInSection:section]);
    UIColor *bgcolor = [UIColor colorWithRed:0.7 green:0.6 blue:0.4 alpha:1.0];
    
	// create the parent view that will hold header Label
    UIView* customView = [[[UIView alloc] initWithFrame:frame] autorelease];

	// create the label object
    //TODO - create an image under this for styling?
	UILabel * headerLabel = [[UILabel alloc] initWithFrame:frame];
	headerLabel.backgroundColor = bgcolor;// colorWithAlphaComponent:0.8];
	headerLabel.opaque = NO;
	headerLabel.textColor = [UIColor whiteColor];
	headerLabel.highlightedTextColor = [UIColor whiteColor];
	headerLabel.font = [UIFont boldSystemFontOfSize:16];
    [headerLabel setShadowColor:[UIColor darkGrayColor]];
    [headerLabel setShadowOffset:CGSizeMake(1.0, 1.0)];
    
	headerLabel.text = [NSString stringWithFormat:@" %@",[self tableView:tableView titleForHeaderInSection:section]]; // i.e. array element
	[customView addSubview:headerLabel];
    
	return customView;
}

/**/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *year = [self.years objectAtIndex:section];
    NSArray *yearEvents = [self.eventsByYear objectForKey:year];
    if(yearEvents) {
        return [yearEvents count];
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 42.0; // 54+ for 2 lines?
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TimelineCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
    }
    
    cell.textLabel.font = [UIFont systemFontOfSize:14.0];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:11.0];

    
    // Configure the cell...
    NSString *cellYear = [self.years objectAtIndex:indexPath.section];
    NSArray *yearEvents = [self.eventsByYear objectForKey:cellYear];
    if(yearEvents && [yearEvents count]>indexPath.row) {
        NSArray *cellEvent = [yearEvents objectAtIndex:indexPath.row];
        cell.textLabel.text = [cellEvent count]?[cellEvent objectAtIndex:0]:nil;
        
        if(([cellEvent count]>1) && [[cellEvent objectAtIndex:1] length]) {
            cell.detailTextLabel.text = [cellEvent objectAtIndex:1];
        } else {
            cell.detailTextLabel.text = @" ";
        }
//        [cell.detailTextLabel setNumberOfLines:2];
        
        UIColor *cellColour = nil;
//        UIImageView *icon = [[UIImageView alloc] init];
  
        UIImageView *icon = cell.imageView;
        icon.image = [UIImage imageNamed:@"190n-bank-narrow"];

        if([cellEvent count]>2) {
            NSString *cellType=[cellEvent objectAtIndex:2];

            if ([cellType isEqualToString:@"war"]) {
//                cellColour = [UIColor colorWithRed:1 green:1 blue:0.9 alpha:1.0];  
                icon.image = [UIImage imageNamed:@"59-flag"];
//                icon.frame = CGRectMake(300, 8, 12, 24);
            } else if ([cellType isEqualToString:@"crypto"]) {
//                cellColour = [UIColor colorWithRed:0.96 green:0.96 blue:1 alpha:1.0];    
                icon.image = [UIImage imageNamed:@"275n-broadcast-narrow"];
//                icon.frame = CGRectMake(300, 7, 12, 26);
            } else {//if ([cellType isEqualToString:@"park"]) {
//                cellColour = [UIColor colorWithRed:0.95 green:1 blue:0.95 alpha:1.0];    
//                icon.image = [UIImage imageNamed:@"96-book"];

            }

            if(cellColour) {
                cell.contentView.backgroundColor = cellColour;
                cell.textLabel.backgroundColor = cellColour;
                cell.detailTextLabel.backgroundColor = cellColour;
            }
        }

    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

@end
