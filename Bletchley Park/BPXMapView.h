//
//  BPXMapView.h
//  Bletchley Park
//
//  Created by Richard George on 05/01/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <MapKit/MapKit.h>

//defined in PCX
#ifdef UseSM3MapView 
@interface BPXMapView : SM3DARMapView
#else
@interface BPXMapView : MKMapView
#endif
@end
