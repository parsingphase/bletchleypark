//
//  BPXMainMenuViewController.m
//  Bletchley Park
//
//  Created by Richard George on 30/12/2011.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "BPXAppDelegate.h"
#import "BPXMainMenuViewController.h"
#import "BPXPendingViewController.h"
#import "BPXSiteMapViewController.h"
#import "BPXTextAreaViewController.h"
#import "BPXTimelineViewController.h"
#import "BPXPeopleViewController.h"

@implementation BPXMainMenuViewController
@synthesize aboutButton;
@synthesize aroundParkButton;
@synthesize visitButton;
@synthesize eventsButton;
@synthesize supportButton;
@synthesize buildLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *buildId = [infoDict valueForKey:@"CFBundleVersion"];  
    
    self.buildLabel.text = [NSString stringWithFormat:@"build %@", buildId];
}

- (void)viewDidUnload
{
    [self setAboutButton:nil];
    [self setAroundParkButton:nil];
    [self setVisitButton:nil];
    [self setEventsButton:nil];
    [self setSupportButton:nil];
    [self setBuildLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [aboutButton release];
    [aroundParkButton release];
    [visitButton release];
    [eventsButton release];
    [supportButton release];
    [buildLabel release];
    [super dealloc];
}

- (IBAction)aboutButtonPressed:(id)sender {
    PSLogDebug(@"aboutButtonPressed");
    BPXTextAreaViewController *textController = nil;
//    UIViewController *firstControllerInTab=nil;
    BPXNavigationController *navController=nil;
    NSMutableArray *controllers=nil;
    controllers = [[[NSMutableArray alloc] init] autorelease];
    NSString *textPath;
    //Pop "first page" controllers of each tab into array here:
    
    textPath = [[NSBundle mainBundle] pathForResource:@"intro" ofType:@"txt"];
    textController = [[[BPXTextAreaViewController alloc] initWithNibName:@"BPXTextAreaViewController" bundle:nil textFile:textPath] autorelease];
    navController =  [[[BPXNavigationController alloc] initWithRootViewController:textController] autorelease];
    textController.title = @"Introduction";
        navController.tabBarItem.image = [UIImage imageNamed:@"190-bank"];
    [controllers addObject:navController];
    
//    textPath = [[NSBundle mainBundle] pathForResource:@"people" ofType:@"txt"];
    BPXPeopleViewController *pvc = [[[BPXPeopleViewController alloc] initWithNibName:@"BPXPeopleViewController" bundle:nil] autorelease];
    navController =  [[[BPXNavigationController alloc] initWithRootViewController:pvc] autorelease];
    pvc.title = @"People";
      navController.tabBarItem.image = [UIImage imageNamed:@"112-group"];
    [controllers addObject:navController];    
    
    textPath = [[NSBundle mainBundle] pathForResource:@"machines" ofType:@"txt"];
    textController = [[[BPXTextAreaViewController alloc] initWithNibName:@"BPXTextAreaViewController" bundle:nil textFile:textPath] autorelease];
    navController =  [[[BPXNavigationController alloc] initWithRootViewController:textController] autorelease];
    textController.title = @"Machines";
       navController.tabBarItem.image = [UIImage imageNamed:@"19-gear"];
    [controllers addObject:navController];    
 /*   
    textPath = [[NSBundle mainBundle] pathForResource:@"timeline" ofType:@"txt"];
    textController = [[[BPXTextAreaViewController alloc] initWithNibName:@"BPXTextAreaViewController" bundle:nil textFile:textPath] autorelease]; */
    BPXTimelineViewController *tlvc = [[[BPXTimelineViewController alloc] initWithNibName:@"BPXTimelineViewController" bundle:nil] autorelease]; 
    navController =  [[[BPXNavigationController alloc] initWithRootViewController:tlvc] autorelease];
    tlvc.title = @"Timeline";
       navController.tabBarItem.image = [UIImage imageNamed:@"55-network"];
    [controllers addObject:navController];
    
    BPXAppDelegate *delegate = (BPXAppDelegate *)[[UIApplication sharedApplication] delegate];

    delegate.tabBarController = [[[UITabBarController alloc] init] autorelease];
    delegate.tabBarController.viewControllers = [NSArray arrayWithArray:controllers];
    
    [self presentModalViewController:delegate.tabBarController animated:YES];

}

- (IBAction)aroundButtonPressed:(id)sender {
    PSLogDebug(@"aboutButtonPressed");
    BPXTextAreaViewController *textController = nil;
    NSString *textPath = nil;
    BPXSiteMapViewController *firstControllerInTab=nil;
    BPXNavigationController *navController=nil;
    NSMutableArray *controllers=nil;
    controllers = [[[NSMutableArray alloc] init] autorelease];
    //Pop "first page" controllers of each tab into array here:
    
    firstControllerInTab = [[[BPXSiteMapViewController alloc] initWithNibName:@"BPXSiteMapViewController" bundle:nil] autorelease];
    navController =  [[[BPXNavigationController alloc] initWithRootViewController:firstControllerInTab] autorelease];
    firstControllerInTab.title = @"Highlights";
    firstControllerInTab.pointsfileName  = @"highlightpoints";
    navController.tabBarItem.image = [UIImage imageNamed:@"28-star"];
    [controllers addObject:navController];    
    
    firstControllerInTab = [[[BPXSiteMapViewController alloc] initWithNibName:@"BPXSiteMapViewController" bundle:nil] autorelease];
    navController =  [[[BPXNavigationController alloc] initWithRootViewController:firstControllerInTab] autorelease];
    firstControllerInTab.title = @"Locations";
    firstControllerInTab.pointsfileName  = @"hutsblocks";
    navController.tabBarItem.image = [UIImage imageNamed:@"07-map-marker"];
    [controllers addObject:navController];    
    
    firstControllerInTab = [[[BPXSiteMapViewController alloc] initWithNibName:@"BPXSiteMapViewController" bundle:nil] autorelease];
    navController =  [[[BPXNavigationController alloc] initWithRootViewController:firstControllerInTab] autorelease];
    firstControllerInTab.title = @"Facilities";
    firstControllerInTab.pointsfileName  = @"facilities";
    navController.tabBarItem.image = [UIImage imageNamed:@"34-coffee"];
    [controllers addObject:navController];
    
    textPath = [[NSBundle mainBundle] pathForResource:@"today" ofType:@"txt"];
    textController = [[[BPXTextAreaViewController alloc] initWithNibName:@"BPXTextAreaViewController" bundle:nil textFile:textPath] autorelease];
    navController =  [[[BPXNavigationController alloc] initWithRootViewController:textController] autorelease];
    textController.title = @"Today";
    navController.tabBarItem.image = [UIImage imageNamed:@"166-newspaper"];
    [controllers addObject:navController];
    
    BPXAppDelegate *delegate = (BPXAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    delegate.tabBarController = [[[UITabBarController alloc] init] autorelease];
    delegate.tabBarController.viewControllers = [NSArray arrayWithArray:controllers];
    
    [self presentModalViewController:delegate.tabBarController animated:YES];
}

- (IBAction)visitButtonPressed:(id)sender {
    
    PSLogDebug(@"aboutButtonPressed");
//    UIViewController *firstControllerInTab=nil;
    BPXTextAreaViewController *textController = nil;
    BPXNavigationController *navController=nil;
    NSMutableArray *controllers=nil;
    controllers = [[[NSMutableArray alloc] init] autorelease];
    NSString *textPath;
    //Pop "first page" controllers of each tab into array here:
    
    textPath = [[NSBundle mainBundle] pathForResource:@"findingus" ofType:@"txt"];
    textController = [[[BPXTextAreaViewController alloc] initWithNibName:@"BPXTextAreaViewController" bundle:nil textFile:textPath] autorelease];
    navController =  [[[BPXNavigationController alloc] initWithRootViewController:textController] autorelease];
    textController.title = @"Finding us";
    navController.tabBarItem.image = [UIImage imageNamed:@"246-route"]; // or 103-map
    [controllers addObject:navController];    
    
    textPath = [[NSBundle mainBundle] pathForResource:@"openingtimes" ofType:@"txt"];
    textController = [[[BPXTextAreaViewController alloc] initWithNibName:@"BPXTextAreaViewController" bundle:nil textFile:textPath] autorelease];
    navController =  [[[BPXNavigationController alloc] initWithRootViewController:textController] autorelease];
    textController.title = @"Opening";
    navController.tabBarItem.image = [UIImage imageNamed:@"83-calendar"];
    [controllers addObject:navController];    
    
    textPath = [[NSBundle mainBundle] pathForResource:@"prices" ofType:@"txt"];
    textController = [[[BPXTextAreaViewController alloc] initWithNibName:@"BPXTextAreaViewController" bundle:nil textFile:textPath] autorelease];
    navController =  [[[BPXNavigationController alloc] initWithRootViewController:textController] autorelease];
    textController.title = @"Prices"; // tickets?
    navController.tabBarItem.image = [UIImage imageNamed:@"299-ticket"]; //172 pricetag
    [controllers addObject:navController];    
    
    textPath = [[NSBundle mainBundle] pathForResource:@"contact" ofType:@"txt"];
    textController = [[[BPXTextAreaViewController alloc] initWithNibName:@"BPXTextAreaViewController" bundle:nil textFile:textPath] autorelease];
    navController =  [[[BPXNavigationController alloc] initWithRootViewController:textController] autorelease];
    textController.title = @"Contact us";
    navController.tabBarItem.image = [UIImage imageNamed:@"75-phone"]; //18-envelope
    [controllers addObject:navController];
    
    
    BPXAppDelegate *delegate = (BPXAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    delegate.tabBarController = [[[UITabBarController alloc] init] autorelease];
    delegate.tabBarController.viewControllers = [NSArray arrayWithArray:controllers];
    
    [self presentModalViewController:delegate.tabBarController animated:YES];
    
}

- (IBAction)eventsButtonPressed:(id)sender {
    PSLogDebug(@"eventsButtonPressed");
    BPXTextAreaViewController *textController = nil;
    BPXNavigationController *navController=nil;
//    NSMutableArray *controllers=nil;
//    controllers = [[[NSMutableArray alloc] init] autorelease];
    NSString *textPath;
    //Pop "first page" controllers of each tab into array here:
    
    textPath = [[NSBundle mainBundle] pathForResource:@"upcoming" ofType:@"txt"];
    textController = [[[BPXTextAreaViewController alloc] initWithNibName:@"BPXTextAreaViewController" bundle:nil textFile:textPath] autorelease];
    
    textController.navigationItem.title = @"Upcoming Events";
    navController =  [[[BPXNavigationController alloc] initWithRootViewController:textController] autorelease];

    [self presentModalViewController:navController animated:YES];
}

- (IBAction)supportButtonPressed:(id)sender {
    PSLogDebug(@"supportButtonPressed");
    UIViewController *firstControllerInTab=nil;
    BPXNavigationController *navController=nil;
    NSMutableArray *controllers=nil;
    controllers = [[[NSMutableArray alloc] init] autorelease];
    //Pop "first page" controllers of each tab into array here:
    
    firstControllerInTab = [[[BPXPendingViewController alloc] initWithNibName:@"BPXPendingViewController" bundle:nil] autorelease];
    navController =  [[[BPXNavigationController alloc] initWithRootViewController:firstControllerInTab] autorelease];
    firstControllerInTab.title = @"Donate";
    navController.tabBarItem.image = [UIImage imageNamed:@"204-wallet"]; // 90-lifebouy
    [controllers addObject:navController];    
    
    firstControllerInTab = [[[BPXPendingViewController alloc] initWithNibName:@"BPXPendingViewController" bundle:nil] autorelease];
    navController =  [[[BPXNavigationController alloc] initWithRootViewController:firstControllerInTab] autorelease];
    firstControllerInTab.title = @"Join Friends";
    navController.tabBarItem.image = [UIImage imageNamed:@"29-heart"];  //243-globe?
    [controllers addObject:navController];    
    
    firstControllerInTab = [[[BPXPendingViewController alloc] initWithNibName:@"BPXPendingViewController" bundle:nil] autorelease];
    navController =  [[[BPXNavigationController alloc] initWithRootViewController:firstControllerInTab] autorelease];
    firstControllerInTab.title = @"Veterans";
    navController.tabBarItem.image = [UIImage imageNamed:@"108-badge"];
    [controllers addObject:navController];
    
    firstControllerInTab = [[[BPXPendingViewController alloc] initWithNibName:@"BPXPendingViewController" bundle:nil] autorelease];
    navController =  [[[BPXNavigationController alloc] initWithRootViewController:firstControllerInTab] autorelease];
    firstControllerInTab.title = @"Volunteer";
    navController.tabBarItem.image = [UIImage imageNamed:@"291-idcard"];
    [controllers addObject:navController];
    
    BPXAppDelegate *delegate = (BPXAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    delegate.tabBarController = [[[UITabBarController alloc] init] autorelease];
    delegate.tabBarController.viewControllers = [NSArray arrayWithArray:controllers];
    
    [self presentModalViewController:delegate.tabBarController animated:YES];
    
    
}

- (IBAction)infoButtonPressed:(id)sender {
    
    PSLogDebug(@"infoButtonPressed");
    BPXTextAreaViewController *textController = nil;
    BPXNavigationController *navController=nil;
    //    NSMutableArray *controllers=nil;
    //    controllers = [[[NSMutableArray alloc] init] autorelease];
    NSString *textPath;
    //Pop "first page" controllers of each tab into array here:
    
    textPath = [[NSBundle mainBundle] pathForResource:@"credits" ofType:@"txt"];
    textController = [[[BPXTextAreaViewController alloc] initWithNibName:@"BPXTextAreaViewController" bundle:nil textFile:textPath] autorelease];

    
    textController.navigationItem.title = @"Info & Credits";
    navController =  [[[BPXNavigationController alloc] initWithRootViewController:textController] autorelease];
    
    [self presentModalViewController:navController animated:YES];
    
}

@end
