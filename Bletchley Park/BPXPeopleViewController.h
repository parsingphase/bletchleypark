//
//  BPXPeopleViewController.h
//  Bletchley Park
//
//  Created by Richard George on 08/01/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BPXPeopleViewController : UITableViewController
{
    NSArray *people_;
}

@property (nonatomic,retain) NSArray *people;
@end
