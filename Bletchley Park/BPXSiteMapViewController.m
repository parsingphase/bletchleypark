//
//  BPXSiteMapViewController.m
//  Bletchley Park
//
//  Created by Richard George on 30/12/2011.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

#import "BPXSiteMapViewController.h"
#import "BPXLocationPoint.h"
#import "PHIGeolocation.h"

@implementation BPXSiteMapViewController
@synthesize mapView;
//@synthesize locationTitleLabel;
@synthesize locationSelectButton;
@synthesize pointsfileName = pointsFileName_;
@synthesize points = points_;
@synthesize loadingPoints = loadingPoints_;
@synthesize locationSelector;
@synthesize directionImageView;
@synthesize locationManager = locationManager_;
@synthesize location = location_;
@synthesize heading = heading_;
@synthesize destinationPoint = destinationPoint_;

#ifdef UseSM3MapView
@synthesize inVrMode = inVrMode_;
#endif

///Pull points out of CSV file into memory
- (void)loadPoints
{
    if(self.pointsfileName) {
        
        NSString *path;
        path = [[NSBundle mainBundle] pathForResource:self.pointsfileName ofType:@"csv"];
        
        if(path) {
            NSError *error = nil;
            
//#warning TODO just use [NSArray arrayWithContentsOfCSVFile:] via NSArray (CHCSVAdditions)
            
            CHCSVParser *pointReader = [[CHCSVParser alloc] initWithContentsOfCSVFile:path encoding:NSUTF8StringEncoding error:&error];
            
            //    NSArray *lineContents;
            [pointReader setParserDelegate:self];
            [pointReader parse];
        } else {
            PSLogDebug(@"%@.csv not found for points loading",self.pointsfileName);
        }
    } else {
        PSLogDebug(@"No points path provided to BPXSiteMapViewController");
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
#ifdef UseSM3MapView
//    [mapView startCamera]; // not by default!
#endif
    [self enableLocationServices];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self disableLocationServices];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //MKCoordinateRegion

    [self scrollMapToDefaultPositionAnimated:NO];
    
//#ifndef UseSM3MapView
    [self loadPoints];
//#endif
    
    if([self.points count]) {
        [self.locationSelectButton setTitle: @"Select a location" forState:UIControlStateNormal];
    } else {
        [self.locationSelectButton setTitle: @"No points for map yet" forState:UIControlStateNormal];
        [self.locationSelectButton setEnabled:NO];
    }
}

- (void)viewDidUnload
{
    [self setDestinationPoint:nil];
    [self setHeading:nil];
    [self setLocation:nil];
    [self setLocationManager:nil];
    [self setLocationSelectButton:nil];
    [self setMapView:nil];
    [self setLocationSelector:nil];
    [self setDirectionImageView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [destinationPoint_ release];
    [heading_ release];
    [locationManager_ release];
    [location_ release];
    [locationSelectButton release];
    [mapView release];
    [locationSelector release];
    [directionImageView release];
    [super dealloc];
}

- (IBAction)locationSelectButtonPressed:(id)sender {
    [self.locationSelector setHidden:NO];
}

#pragma mark - CHCSVParserDelegate methods

- (void) parser:(CHCSVParser *)parser didStartDocument:(NSString *)csvFile
{
    //reset
    self.loadingPoints = [[[NSMutableArray alloc] init] autorelease];
    PSLogDebug(@"parser didStartDocument");
}

- (void) parser:(CHCSVParser *)parser didStartLine:(NSUInteger)lineNumber
{
    PSLogDebug(@"parser didStartLine");
    [self.loadingPoints addObject:[[[NSMutableArray alloc] init] autorelease]]; // add a new array to hold new line
}

- (void) parser:(CHCSVParser *)parser didEndLine:(NSUInteger)lineNumber
{
    PSLogDebug(@"parser didEndLine");
}

- (void) parser:(CHCSVParser *)parser didReadField:(NSString *)field
{
//    PSLogDebug(@"parser didReadField");
    if([self.loadingPoints count]) { // damn well should be loaded!
        NSMutableArray *lineArray = [self.loadingPoints lastObject];
        if(lineArray) { //again, really should exist
            [lineArray addObject:field];
        }
    }
}

- (void) parser:(CHCSVParser *)parser didEndDocument:(NSString *)csvFile
{
    PSLogDebug(@"parser didEndDocument");
    //convert everything to a useful format in self.points and flush out the working copy
//    PSLogDebug(@"%@",[self.loadingPoints description]);
    BPXLocationPoint *point=nil, *lastpoint = nil;
    NSString * coordString;
    
    NSMutableArray *tmpArray = [[NSMutableArray alloc] initWithCapacity:[self.loadingPoints count]];
    for (NSArray *lineArray in self.loadingPoints) {
        if([lineArray count] >= 3) {
            
//#warning TODO: Group points with the same name in an array. 
// dictionary loses order!
            if ([[lineArray objectAtIndex:0] length]) {
                
                point = [[BPXLocationPoint alloc]init];
                point.name = [lineArray objectAtIndex:0];
                coordString = [lineArray objectAtIndex:1];
                point.latitude = [NSNumber numberWithFloat:[coordString floatValue]];
                coordString = [lineArray objectAtIndex:2];
                point.longitude = [NSNumber numberWithFloat:[coordString floatValue]];
            
                // gather batches of points with same name:
                if(lastpoint && [point.name isEqualToString:lastpoint.name]) {
                    if([[tmpArray lastObject] isKindOfClass:[NSMutableArray class]]) {
                        [((NSMutableArray *)[tmpArray lastObject]) addObject:point];
                    } else {
                        [tmpArray replaceObjectAtIndex:[tmpArray count]-1 withObject:[NSMutableArray arrayWithObjects:[tmpArray lastObject], point, nil]];
                    }
                } else {
                    [tmpArray addObject:point];
                }
                
                lastpoint = point;
                [point release];
            }
        } else {
            PSLogDebug(@"Empty line in points parser");
//            PSLogDebug(@"Bad line length in points parser");
        }
    }
    self.points = [NSArray arrayWithArray:tmpArray]; // de-mutable
    [tmpArray release];
    PSLogDebug(@"%@",[self.points description]);

}

- (void) parser:(CHCSVParser *)parser didFailWithError:(NSError *)error
{
    PSLogDebug(@"parser didFailWithError");
}

#pragma mark - Picker delegate methods
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView 
{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component 
{
    return [self.points count]+1; // 1 for blank!
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component 
{
    
    BPXLocationPoint *point;
    if(row==0) {
        return @""; // empty first
    }
    row--; // otherwise an array offset
    if ([self.points count]> row) {
        if ([[self.points objectAtIndex:row] isKindOfClass:[NSArray class]]) {
            point = [((NSArray *)[self.points objectAtIndex:row]) objectAtIndex:0];
        } else {
            point = [self.points objectAtIndex:row];
        }
        return point.name;
    }
    return @"-";
}

//- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view;

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (row) { // if zero, we've hit the blank - clear all points?
        row--; // get an array index
        if([self.points count]> row) {
            [self.mapView removeAnnotations:self.mapView.annotations]; //clear all!
            
            if([[self.points objectAtIndex:row] isKindOfClass:[NSArray class]]) {
                NSArray *pointsArray = [self.points objectAtIndex:row];
                [self.mapView addAnnotations:pointsArray];
                [self.directionImageView setHidden:YES];
                self.destinationPoint = nil;
                [self.locationSelectButton setTitle:[[pointsArray objectAtIndex:0] name] forState:UIControlStateNormal];
                [self scrollMapToDefaultPositionAnimated:YES];

            } else {
                
                self.destinationPoint = [self.points objectAtIndex:row];
                PSLogDebug(@"Selected point: %@", self.destinationPoint);
                [self.locationSelectButton setTitle: self.destinationPoint.name forState:UIControlStateNormal] ;
                
                [self.mapView addAnnotation:self.destinationPoint];
                [self scrollMapToIncludePoint:self.destinationPoint];
            }
        }
        
        if(self.locationManager) { // treat change of point as a device move if location's working
            [self deviceDidMoveOrTurn];
        }
    } else {
        [self.directionImageView setHidden:YES];
    }
    [pickerView setHidden:YES];
}

- (MKAnnotationView *)mapView:(MKMapView *)aMapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    static NSString *PinIdentifier = @"PinIdentifier";
    
    // If this is the user location, stick with default
    if([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    }
    
    // Obtain a pin
    MKPinAnnotationView *pin = (MKPinAnnotationView *)[aMapView
                                                        dequeueReusableAnnotationViewWithIdentifier:PinIdentifier];
    
    if(pin == nil) 
    {
        pin = [[[MKPinAnnotationView alloc] initWithAnnotation:annotation
                                               reuseIdentifier:PinIdentifier]
               autorelease];
    }
    
    //Configure the pin
    pin.annotation=annotation;
    pin.animatesDrop=YES;
    pin.pinColor=MKPinAnnotationColorRed;
    pin.canShowCallout=NO;
    
    PSLogDebug(@"viewForAnnotation");
    return pin;
}

#pragma mark - SM3MapView delegate methods
#ifdef UseSM3MapView
- (void) sm3darLoadPoints:(SM3DARController *)sm3dar;
{
    [self.mapView addAnnotations:self.points];
    [self.mapView zoomMapToFitPointsIncludingUserLocation:YES];

}
#endif

- (IBAction)tappedViewToggleButton:(id)sender {
#ifdef UseSM3MapView
    if(self.inVrMode) {
        [mapView stopCamera];
         [self.mapView removeAnnotations:self.mapView.annotations]; 
    } else {
        [mapView startCamera];
        [self.mapView addAnnotations:self.points];
    }
    self.inVrMode = !self.inVrMode;
#endif
}

#pragma mark - position display methods
- (void)deviceDidMoveOrTurn
{
    CLLocationDirection destinationBearing =0;
    
    if(self.location && self.destinationPoint) { // should possibly check self.bearing but that can legitimately be 0
        destinationBearing = [PHIGeolocation greatCircleBearingFromLocation:self.location.coordinate
                                                                 toLocation:self.destinationPoint.coordinate];
        
        CLLocationDirection deviceBearingInRadians = M_PI*self.heading.trueHeading/180;
        self.directionImageView.transform = CGAffineTransformMakeRotation(2*M_PI + destinationBearing - deviceBearingInRadians);
        [self.directionImageView setHidden:NO];
    }
}

- (void) scrollMapToDefaultPositionAnimated:(BOOL) animated
{
    CLLocationCoordinate2D centre = CLLocationCoordinate2DMake(51.9972,-0.742);
    MKCoordinateRegion region=MKCoordinateRegionMakeWithDistance(centre, 250,250);
    
    [self.mapView setRegion:region animated:animated];
}

- (void) scrollMapToIncludePoint: (BPXLocationPoint *)point
{
    // area of interest: don't display the user if they're outside it!
    /*
     MIN 51.9954502980349	-0.743780281832031
     MAX 51.9985544856047	-0.736340852145092
     */
    
    CLLocationCoordinate2D ownPosition, mapCentrePosition;
    
    if(self.location 
       && (self.location.coordinate.latitude > 51.995)  
       && (self.location.coordinate.latitude < 51.999)  
       && (self.location.coordinate.longitude > -0.744)  
       && (self.location.coordinate.longitude < -0.737)) {
        // use self.location as one focal point
        
        ownPosition = self.location.coordinate;
    } else {
        //use park centre as the focal point
        // Park Centre,51.9972,-0.742
        ownPosition = CLLocationCoordinate2DMake(51.997,-0.742);
    }
    
//    ownPosition = CLLocationCoordinate2DMake(51.99855448560470,-0.7433192417015590);// TMNOC shop
    
    mapCentrePosition= CLLocationCoordinate2DMake(
                            (point.coordinate.latitude + ownPosition.latitude)/2,
                            (point.coordinate.longitude + ownPosition.longitude)/2);
    
    double pointsSeparationDistance = [PHIGeolocation greatCircleDistanceFromLocation:ownPosition toLocation:point.coordinate onSphereOfRadiusMetres:[PHIGeolocation earthMeanRadiusMetres]];
    // 425m from park centre to station 
    
    CLLocationDistance mapRadius = MAX(pointsSeparationDistance, 250);
    
    PSLogDebug(@"Zoom to mapradius %f", mapRadius);
    
    MKCoordinateRegion region=MKCoordinateRegionMakeWithDistance(mapCentrePosition, mapRadius,mapRadius);
    
    [self.mapView setRegion:region animated:YES];  
    [self.mapView setNeedsLayout];
}


#pragma mark - Location delegate methods
- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)heading {
    self.heading = heading;
    [self deviceDidMoveOrTurn];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    self.location= newLocation;
    [self deviceDidMoveOrTurn];
}

#pragma mark - location manager operations

- (void) enableLocationServices
{
    
    if(self.locationManager == nil) {
        self.locationManager = [[[CLLocationManager alloc] init] autorelease];
        
        
        // check if the hardware has a compass
        
        if ([CLLocationManager headingAvailable] == NO) {
            
            // No compass is available. Fine; just don't try and show arrows           
            self.locationManager = nil;
            PSLogDebug(@"No compass so not setting up location manager");
            
        } else {
            
            // heading service configuration
            self.locationManager.headingFilter = 1;
            
            // setup delegate callbacks
            self.locationManager.delegate = self;
            
            self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; //kCLLocationAccuracyNearestTenMeters;
            self.locationManager.distanceFilter= 10.0; //update frequency (distance)
            
            // start the compass
            [self.locationManager startUpdatingHeading];
            [self.locationManager startUpdatingLocation];
            
        }
    }
}

- (void) disableLocationServices
{
    if(self.locationManager != nil) {
        [self.locationManager stopUpdatingHeading];
        [self.locationManager stopUpdatingLocation];
        
        self.locationManager = nil;
    }
}

@end
