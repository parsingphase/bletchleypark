//
//  BPXLocationPoint.m
//  Bletchley Park
//
//  Created by Richard George on 03/01/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BPXLocationPoint.h"

@implementation BPXLocationPoint

@synthesize name = name_;
@synthesize latitude = latitude_;
@synthesize longitude = longitude_;

- (NSString *) description
{
    return [NSString stringWithFormat:@"%@: %@,%@", self.name, self.latitude, self.longitude];
}

- (CLLocationCoordinate2D) coordinate
{
    return CLLocationCoordinate2DMake(self.latitude.doubleValue, self.longitude.doubleValue);
}

- (NSString *)title {
    return self.name;
}
@end
