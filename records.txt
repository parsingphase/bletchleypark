Initial menu page? (mansion high-gamma background?)
 Then to tabbed nav?

"About Bletchley Park"
 History / People / Machines / Timeline

"At the park"
  Highlights / Locations / Facilities / What's on today

"Planning a visit"
  Finding us / opening / prices 

"What's On"
   Upcoming events

"Support Us"
  Friends / Volunteer / Veterans?

"Pocket Enigma" - check the name's not taken!


Segment control 1a/b/c - in menu bar? Remember state?



1a    // "Highlights"        - Star
 "About Bletchley"

     Polish Memorial
     Colossus
     Enigma display
     Turing statue
     Bombe

1b   //Places            - House? Signpost? Map?
        - Direct link to map? (in title bar?)
        - highlights in subtitle w/star? :/
    
    Mansion
    Cottage
    Polish Memorial
    Hut 1
    Hut 3
    Hut 4
    Hut 6
    Hut 8
    Hut 11
    Hut 12
    Block B
    Shop
    Block H / TNMoC
    Post Office
    Toy Collection
    Cinema
    Churchill Collection
    Model Railway
    Garage
    Maritime display
    American Garden Trail
    
1c  //Facilities

    
2    //Events / opening/ talks  - Calendar (by pawprint!)
    Events - read ical?
    
    Combine 2, 3 later?
    
3   //Visiting - directions, (link to) opening times      - Singpost? Compass? 'treasure map' (squigly line - X)
        Contact - phone number, email clickable
        Special URL format to open / get directions in google maps app?
        Group visits!
        
    Trains
    http://www.bletchleypark.org.uk/content/museum1/openingtimes.rhtm
    http://www.bletchleypark.org.uk/content/museum1/AddminssionPrices.rhtm
    
    Weather?
    
4    //Timeline? (keydates in wiki) - Triple Arrow? Bulletpoints? Book?

5 // Support: Donate / join / volunteer?         - heart? Museum? 
 Both Bletchley Park AND TNMoC
 

//Enigma decoding in app?

    // (Machines?)
    Enigma
    Tunny
    Bombe
    Colossus
    
        
    //People - later phase!     -  multi-person icon
    Turing
    Denniston
    Cunningham
    Knox
    Welchman
    Bertrand
    Zyglaski
    Alexander
    Sinclair
    Rozycki
    Herivel
    Tiltman
    Rejewski
    Newman
    Flowers
    Tutte
    Churchill
    Ridley
    Fleming
    Tutte
     // Keys, machines, methods via people, places only? Search page?
     
  Books?    